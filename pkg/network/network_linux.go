package network

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/pkg/errors"
)

func AssignIPToDevice(name string, ip string) error {
	b, err := exec.Command("ip", "addr", "add", ip, "dev", name).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "could not assign ip to device (%v), [stdout/stderr]: %s", err, b)
	}
	return nil
}

func TurnOnDevice(name string) error {
	b, err := exec.Command("ip", "link", "set", name, "up").CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "could not turn on device (%v), [stdout/stderr]: %s", err, b)
	}
	return nil
}

func RouteAllowedIPsToDevice(name string, allowedIps []string) error {
	for _, ip := range allowedIps {
		b, err := exec.Command("ip", "route", "add", ip, "dev", name).CombinedOutput()
		if err != nil {
			errors.Wrapf(err, "could not route allowed ips to device (%v), [stdout/stderr]: %s", err, b)
		}
	}
	return nil
}

func ModifySystemDNS(interfaceName string, dns []string, dnsSearch []string) error {
	// #PostUp = resolvectl dns %i 10.10.10.1; resolvectl domain %i \~svc.cluster.local

	// c := exec.Command("resolvconf", "-a", interfaceName)
	// stdin := bytes.NewBuffer(nil)
	//
	// fmt.Fprintf(stdin, "search %s\n", strings.Join(dnsSearch, " "))
	// for i := range dns {
	// 	fmt.Fprintf(stdin, "nameserver %s\n", dns[i])
	// }
	//
	// c.Stdin = stdin

	c := exec.Command("resolvectl", "dns", interfaceName, dns[0])
	b, err := c.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "could not modify system dns (%v), [stdout/stderr]: %s", err, b)
	}

	c = exec.Command("resolvectl", "domain", interfaceName, fmt.Sprintf("%s", "svc.cluster.local"))
	b, err = c.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "could not modify system dns (%v), [stdout/stderr]: %s", err, b)
	}

	// b, err := exec.Command("nmcli", "con", "mod", "test_network", "ipv4.dns", strings.Join(dns, ",")).CombinedOutput()
	// // b, err := exec.Command("resolvconf", "-a", "tun0").CombinedOutput()
	// if err != nil {
	// 	return errors.Wrapf(err, "could not modify system dns (%v), [stdout/stderr]: %s", err, b)
	// }
	//
	// b, err = exec.Command("nmcli", "con", "mod", "test_network", "ipv4.dns-search", strings.Join(dnsSearch, ",")).Output()
	// if err != nil {
	// 	return errors.Wrapf(err, "could not modify system dns search (%v), [stdout/stderr]: %s", err, b)
	// }

	// b, err = exec.Command("systemctl", "restart", "NetworkManager").Output()
	// if err != nil {
	// 	return errors.Wrapf(err, "could not restart NetworkManager (%v), [stdout/stderr]: %s", err, b)
	// }

	return nil
}

func RestoreSystemDNS(interfaceName string) error {
	b, err := exec.Command("resolvconf", "-d", interfaceName).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "could not restore system dns (%v), [stdout/stderr]: %s", err, b)
	}
  return nil
}
