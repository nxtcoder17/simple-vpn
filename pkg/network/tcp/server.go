package tcp

import (
	"net"
	"sync"

	"github.com/nxtcoder17/simple-vpn/pkg/logging"
	"github.com/nxtcoder17/simple-vpn/pkg/types"
)

type Server struct {
	listener net.Listener
	wg       sync.WaitGroup
	logger   logging.Logger
}

func (s *Server) StartListening(onConnect types.OnConnectFunc) error {
	for {
		s.logger.Infof("waiting for new connections, at %s", s.listener.Addr().String())
		c, err := s.listener.Accept()
		s.wg.Add(1)
		go func() {
			defer s.wg.Done()
			onConnect(c, err)
		}()
	}
}

func (s *Server) StopListening() error {
	s.logger.Infof("waiting for existing connections to finish")
	s.wg.Wait()
	return s.listener.Close()
}

type ServerOpts struct {
	Logger logging.Logger
}

func NewServer(addr string, opts ServerOpts) (*Server, error) {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	return &Server{
		listener: l,
		wg:       sync.WaitGroup{},
		logger:   opts.Logger,
	}, nil
}
