package network

import (
	"github.com/songgao/water"
)

type Device struct {
	*water.Interface
}

func CreateTunDevice() (*Device, error) {
	config := water.Config{
		DeviceType: water.TUN,
	}

	iface, err := water.New(config)
	if err != nil {
		return nil, err
	}

	return &Device{
		Interface: iface,
	}, nil
}
