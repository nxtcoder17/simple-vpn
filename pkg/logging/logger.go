package logging

import (
	"fmt"
	"os"

	"github.com/charmbracelet/log"
)

type Logger interface {
	AppendPrefix(prefix string) Logger
	Debugf(msg string, args ...any)
	Infof(msg string, args ...any)
	Fatalf(msg string, args ...any)
	Fatal(err error)
	Errorf(err error, msg string, args ...any)
	Error(err error)
	Warnf(msg string, args ...any)
}

type logger struct {
	Logger *log.Logger
}

// Fatal implements Logger.
func (l *logger) Fatal(err error) {
	l.Logger.Fatal(err)
}

// Debugf implements Logger.
func (l *logger) Debugf(msg string, args ...any) {
	l.Logger.Debugf(msg, args...)
}

// Error implements Logger.
func (l *logger) Error(err error) {
	l.Logger.Error(err)
}

// Errorf implements Logger.
func (l *logger) Errorf(err error, msg string, args ...any) {
	l.Logger.Errorf(err.Error())
}

// Warnf implements Logger.
func (l *logger) Warnf(msg string, args ...any) {
	l.Logger.Warnf(msg, args...)
}

// Fatalf implements Logger.
func (l *logger) Fatalf(msg string, args ...any) {
	l.Logger.Fatalf(msg, args...)
}

// AppendPrefix implements Logger.
func (l *logger) AppendPrefix(prefix string) Logger {
	l2 := l.Logger.WithPrefix(fmt.Sprintf("%s:%s", l.Logger.GetPrefix(), prefix))
	return &logger{Logger: l2}
}

// Infof implements Logger.
func (l *logger) Infof(msg string, args ...any) {
	l.Logger.Infof(msg, args...)
}

type Opts struct {
	Prefix string
}

func NewLogger(opts Opts) Logger {
	lgr := log.NewWithOptions(os.Stdout, log.Options{
		Level:           0,
		Prefix:          opts.Prefix,
		ReportTimestamp: false,
		ReportCaller:    true,
		CallerOffset:    1,
		Fields:          []interface{}{},
		Formatter:       log.TextFormatter,
	})

	return &logger{
		Logger: lgr,
	}
}
