package types

import "net"

type Client struct {
	IP   string
	Name string
	net.Conn
}

type OnConnectFunc func(conn net.Conn, err error)

type ClientHello struct {
	Name string `json:"name"`
	IP   string `json:"ip"`
}

type ClientHelloResponse struct {
	OK bool `json:"ok"`
}
