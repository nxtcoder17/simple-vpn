package main

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"flag"
	"io"
	"log"
	"net"

	"github.com/nxtcoder17/simple-vpn/pkg/logging"
	"github.com/nxtcoder17/simple-vpn/pkg/network"
	"github.com/nxtcoder17/simple-vpn/pkg/types"
)

func main() {
	var addr string
	var ip string
	flag.StringVar(&addr, "addr", "", "--addr <addr>")
	flag.StringVar(&ip, "ip", "", "--ip <ip>")
	flag.Parse()

	logger := logging.NewLogger(logging.Opts{
		Prefix: "client",
	})

	device, err := network.CreateTunDevice()
	if err != nil {
		logger.Fatal(err)
	}

	if err = network.AssignIPToDevice(device.Name(), ip); err != nil {
		logger.Fatal(err)
	}

	if err := network.TurnOnDevice(device.Name()); err != nil {
		logger.Fatal(err)
	}

	if err := network.RouteAllowedIPsToDevice(device.Name(), []string{"10.13.0.0/16"}); err != nil {
		logger.Fatal(err)
	}

	conn, err := net.Dial("tcp", addr)
	if err != nil {
		logger.Fatal(err)
	}

	ch := types.ClientHello{
		Name:      "client-" + ip,
		IP:        ip,
		AuthToken: "asddskfjasdfjasdfsa;df",
	}

	b, err := json.Marshal(ch)
	if err != nil {
		logger.Fatal(err)
	}

	msgLen := make([]byte, 4)
	binary.LittleEndian.PutUint32(msgLen, uint32(len(b)))

	if _, err := conn.Write(append(msgLen, b...)); err != nil {
		logger.Fatal(err)
	}

	go func() {
		for {
			message := make([]byte, 0xffff)
			n, err := conn.Read(message)
			if err != nil {
				if errors.Is(err, io.EOF) {
					log.Fatalf("client disconnected, with err: %v", err)
				}
				logger.Error(err)
			}

			logger.Infof("writing %d bytes to device", n)
			device.Write(message[:n])
		}
	}()

	message := make([]byte, 0xffff)
	for {
		n, err := device.Read(message)
		if err != nil {
			if errors.Is(err, io.EOF) {
				log.Fatalf("client disconnected, with err: %v", err)
			}
			logger.Error(err)
		}

		logger.Infof("writing %d bytes to server", n)
		conn.Write(message[:n])
	}
}
