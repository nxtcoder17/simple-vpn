package main

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"net"
	"strings"
	"time"

	"github.com/nxtcoder17/simple-vpn/pkg/logging"
	"github.com/nxtcoder17/simple-vpn/pkg/network"
	"github.com/nxtcoder17/simple-vpn/pkg/network/tcp"
	"github.com/nxtcoder17/simple-vpn/pkg/types"
)

func main() {
	var ip string
	var addr string
	flag.StringVar(&ip, "ip", "", "--ip <ip>")
	flag.StringVar(&addr, "addr", "", "--addr <addr>")
	flag.Parse()

	logger := logging.NewLogger(logging.Opts{
		Prefix: "server",
	})

	device, err := network.CreateTunDevice()
	if err != nil {
		logger.Fatal(err)
	}

	if err = network.AssignIPToDevice(device.Name(), ip); err != nil {
		logger.Fatal(err)
	}

	if err := network.TurnOnDevice(device.Name()); err != nil {
		logger.Fatal(err)
	}

	s, err := tcp.NewServer(addr, tcp.ServerOpts{Logger: logger})
	if err != nil {
		logger.Fatal(err)
	}

	defer func() {
		if err := s.StopListening(); err != nil {
			logger.Fatal(err)
		}
	}()

	clients := map[string]types.Client{}
	go func() {
		for {
			<-time.After(2 * time.Second)
			names := make([]string, len(clients))
			for k := range clients {
				names = append(names, k)
			}

			logger.Infof("clients: \n\t%v", strings.Join(names, "\n\t"))
		}
	}()

	s.StartListening(func(conn net.Conn, err error) {
		msgLenBuff := make([]byte, 4)
		if _, err := conn.Read(msgLenBuff); err != nil {
			logger.Error(err)
		}

		msgLen := binary.LittleEndian.Uint32(msgLenBuff)
		ch := make([]byte, int(msgLen))
		if _, err := conn.Read(ch); err != nil {
			logger.Error(err)
			return
		}

		var clientHello types.ClientHello
		if err := json.Unmarshal(ch, &clientHello); err != nil {
			logger.Error(err)
			return
		}

		client := types.Client{
			IP:   clientHello.IP,
			Name: clientHello.Name,
			Conn: conn,
		}
		clients[client.IP] = client

		logger := logger.AppendPrefix(client.Name)
		if err != nil {
			logger.Error(err)
			return
		}
		defer client.Close()
		for {
			logger.Infof("tcp connection listening")
			message := make([]byte, 0xffff)
			for {
				n, err := client.Read(message)
				if err != nil {
					if errors.Is(err, io.EOF) {
						logger.Infof("client closed connection, with err: %v", err)
						delete(clients, client.IP)
						return
					}
				}
				// 1. tcp/ip standard packet size
				// 2. tcp/ip header => destination addr
				ih, err := ReadIPv4Header(message[:n])
				if err != nil {
					continue
				}

				if ih.DstAddr.String() != ip {
					// this packet is destined for another client/server
					dc, ok := clients[ih.DstAddr.String()]
					if !ok {
						logger.Warnf("no client found for ip: %s", ih.DstAddr.String())
						// no client found, drop packet
						continue
					}

					logger.Infof("writing %d bytes to destination client", n)
					dc.Write(message[:n])
					continue
				}

				logger.Infof("ip header: %+v\n", ih)

				if device != nil {
					logger.Infof("interface writing %d bytes", n)
					logger.Infof(string(message[:n]))
					_, err = device.Write(message[:n])
					if err != nil {
						if errors.Is(err, io.EOF) {
							logger.Infof("client closed connection, with err: %v", err)
							return
						}
						logger.Infof("interface write error: %v", err)
						continue
					}
					logger.Debugf("interface write done")
				}
			}
		}
	})

	logger.Infof("device tunnel (%s) listening on ip (%s)", device.Name(), ip)
}
