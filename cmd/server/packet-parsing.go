package main

import (
	"encoding/binary"
	"fmt"
	"net"
)

// IPv4Header represents an IPv4 packet header
type IPv4Header struct {
	Version  uint8
	IHL      uint8
	TOS      uint8
	Length   uint16
	ID       uint16
	Flags    uint8
	Offset   uint16
	TTL      uint8
	Protocol uint8
	Checksum uint16
	SrcAddr  net.IP
	DstAddr  net.IP
}

// ReadIPv4Header parses an IPv4 header from a byte slice
func ReadIPv4Header(data []byte) (*IPv4Header, error) {
	if len(data) < 20 {
		return nil, fmt.Errorf("data is too short to contain an IPv4 header")
	}

	var header IPv4Header
	header.Version = data[0] >> 4
	header.IHL = data[0] & 0x0F
	header.TOS = data[1]
	header.Length = binary.BigEndian.Uint16(data[2:4])
	header.ID = binary.BigEndian.Uint16(data[4:6])
	flagFrag := binary.BigEndian.Uint16(data[6:8])
	header.Flags = uint8(flagFrag >> 13)
	header.Offset = flagFrag & 0x1FFF
	header.TTL = data[8]
	header.Protocol = data[9]
	header.Checksum = binary.BigEndian.Uint16(data[10:12])
	header.SrcAddr = net.IP(data[12:16])
	header.DstAddr = net.IP(data[16:20])

	return &header, nil
}
